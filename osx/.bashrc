HOST_COLOR=$COLOR_LIGHT_GREEN
USER_COLOR=$COLOR_LIGHT_PURPLE

#------------------------------------------------------------
#  completion - 補完
#------------------------------------------------------------
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f `brew --prefix`/etc/bash_completion ]; then
    . `brew --prefix`/etc/bash_completion
fi

#------------------------------------------------------------
#  keychain
#------------------------------------------------------------
HOST=$(hostname)
keychain --nogui --quiet ~/.ssh/id_rsa
source ~/.keychain/$HOST-sh