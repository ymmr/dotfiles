# Export environment variable

COLOR_RESET="\[\033[00m\]"

COLOR_BLACK="\[\033[0;30m\]"
COLOR_BLUE="\[\033[0;34m\]"
COLOR_GREEN="\[\033[0;32m\]"
COLOR_CYAN="\[\033[0;36m\]"
COLOR_RED="\[\033[0;31m\]"
COLOR_PURPLE="\[\033[0;35m\]"
COLOR_LIGHT_GRAY="\[\033[0;37m\]"
COLOR_DARK_GRAY="\[\033[1;30m\]"
COLOR_LIGHT_BLUE="\[\033[1;34m\]"
COLOR_LIGHT_GREEN="\[\033[1;32m\]"
COLOR_LIGHT_CYAN="\[\033[1;36m\]"
COLOR_LIGHT_RED="\[\033[1;31m\]"
COLOR_LIGHT_PURPLE="\[\033[1;35m\]"
COLOR_YELLOW="\[\033[1;33m\]"
COLOR_WHITE="\[\033[1;37m\]"

COLOR_TST="\[\033[38;05;116m\]"

# Call local bashrc
if [ `uname` = "Darwin" ]; then
  # for OSX
  source ~/dotfiles/osx/.bashrc
elif [ `uname` = "Linux" ]; then
  # for Linux
  source ~/dotfiles/linux/.bashrc
fi


# Call common bashrc
source ~/.bashrc

if [ -z $STY ]; then
	# Not Screen
	SCREEN_SESSION="ymsc"

	screen -ls | grep -v $SCREEN_SESSION | grep Detached | cut -d. -f1 | awk '{print $1}' | xargs kill
    
    if [ -n "$1" ]; then
        screen -x $SCREEN_SESSION || screen -r $SCREEN_SESSION || screen -S $SCREEN_SESSION    
    else
        echo "usage: s screen_name"  
        return 1
    fi
else
    export PROMPT_COMMAND='echo -ne "\033k\033\0134\033k$(basename $(pwd))\033\\"'
fi


export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
