#!/bin/sh
#
cd $(dirname $0)
#dotfile関連設定
for dotfile in .?*
do
  if [ $dotfile != '..' ] && [ $dotfile != '.git' ] && [ $dotfile != '.gitignore' ]
  then
    ln -fs "$PWD/$dotfile" $HOME
  fi
done


if [ `uname` = "Darwin" ]; then
	ln -fs "$PWD/osx/.screenrc" $HOME
  # source ~/dotfiles/src/.bashrc.osx
elif [ `uname` = "Linux" ]; then
	ln -fs "$PWD/linux/.screenrc" $HOME
fi